<?php

echo("Distance du Zombie 1 ?" . "\n");
$distanceA = trim(fgets(STDIN));
echo("Distance du Zombie 2 ?" . "\n");
$distanceB = trim(fgets(STDIN));

echo(afficheNomProche($distanceA, $distanceB, "Zombie 1", "Zombie 2"));

function afficheNomProche ($distanceA, $distanceB, $nomA, $nomB){
    if ($distanceA == $distanceB){
        return $nomA . " et " . $nomB . " sont à la même distance.";
    } else if ($distanceA < $distanceB){
        return $nomA . " est le plus proche.";
    } else {
        return $nomB . " est le plus proche.";
    }
}