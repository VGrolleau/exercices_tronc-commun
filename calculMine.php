<?php
// Calcul trajectoire de la mine, par Julien THOMAS <3.

// Lancement du calcul de trajectoire avec les valeurs du Robot X et Y, et de la Mine X et Y. Ces valeurs sont modifiables !
chemin(12, 4, 7, 8);


/*
*
*	Fonction qui envoie la direction de la mine par rapport au robot. (Par exemple : SO)
*   Renvoie rien si le robot est sur la mine.
*
*/
function positionMine($Rx, $Ry, $Mx, $My) {

	// Initialisation des variables.
	$NSPos = "";
	$EOPos = "";

	// Condition raccourcie en une seule ligne qui compare les positions X.
	$EOPos = ($Rx < $Mx ? "E" : ($Rx > $Mx ? "O" : ""));
	// Condition raccourcie en une seule ligne qui compare les positions Y.
	$NSPos = ($Ry < $My ? "N" : ($Ry > $My ? "S" : ""));

	// Renvoie la direction de la mine.
	return $NSPos . $EOPos . "";
}

/*
*
*	Fonction qui calcule le chemin pour arriver à la mine.
*
*/
function chemin($posRx, $posRy, $posMx, $posMy) {

	// Création d'une boucle infinie
	while (true) {

		// Stockage de la postion de la mine dans la variable $position.
		$position = positionMine($posRx, $posRy, $posMx, $posMy);

		// Affichage de la position actuelle du robot (C'est uniquement du débug, si tu ne veux pas le voir ajoute // au tout début de la ligne)
		echo "POS : " . ($position == "" ? "DESSUS" : $position) . " (rx:" . $posRx . ", ry:" . $posRy . ", mx:" . $posMx . ", my:" . $posMy . ")\n";

		// Si la $position retourne rien (le robot est sur la bombe), alors on dit qu'on est dessus et on arrête la boucle !
		if ($position == "") {
			echo "Sur la bombe !";
			return;
		}

		// Initialisation de la variable $moving qui va nous servir a afficher les étapes du chemin du robot vers la mine.
		$moving = "";

		/*
		*
		* strpos($chaineDeCaractere, $caractereRecherché) : Permet de retourner un booléen VRAI si ma chaine de caractère
		*   ($chaineDeCaractere) contient le caractère recherché ($caractèreRecherché).
		*
		* Par exemple : Je veux vérifier si ma chaîne de caractère : "Hello world !" content "Hello".
		* J'ai juste a faire : 
		*
		*  if (strpos("Hello world !", "Hello") !== false) {
	    *      echo "Je contient hello !"; 
		*  }
		*
		*  NOTE : le "!== false" me sert à vérifier si "Hello" est dans "Hello world !", ne cherches pas.
		*
		*/

		// Ducoup, si dans ma position je retrouve 'N'
		if (strpos($position, 'N') !== false) {
			// Je bouge le Robot sur l'axe Y de manière positive en ajoutant 1 à sa position.
			$posRy++; // $posRy + 1; fonctionne aussi.
			// J'enregistre "N" dans $moving pour l'afficher plus tard.
			$moving .= "N";
		} 

		// Ici c'est la même chose qu'au dessus sauf que c'est pour 'S';
		if (strpos($position, 'S') !== false) {
			// Idem ici sauf que c'est l'inverse.
			$posRy--;
			// Là pareil j'ajoute "S" à $moving;
			$moving .= "S";
		}

		// Les deux autres conditions sont identiques à celles du dessus, sauf que c'est pour l'Est et l'Ouest, donc axe X.
		if (strpos($position, 'E') !== false) {
			$posRx++;
			$moving .= "E";
		} 
		if (strpos($position, 'O') !== false) {
			$posRx--;
			$moving .= "O";
		}

		// Affichage de l'étape de mouvement du robot.
		echo "Le robot a bougé en " . $moving . " !\n";
	}
}